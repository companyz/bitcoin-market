package controllers

import play.api.mvc.Controller
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import akka.util.Timeout
import akka.pattern.ask
import models.{MarketSpread, CurrencyPair}
import actors.ExchangeAPI.{Exchange, CurrencyPairsRequest}
import akka.actor.ActorRef
import actors.storage.MarketSpreadQueryingActor.{AllRecords, MarketSpreadsResponse, MarketSpreadQuery}
import actors.ExchangeApiActor.ExchangeListRequest
import org.joda.time.DateTime
import com.github.nscala_time.time.Imports._
import java.util.concurrent.TimeUnit
import models.CurrencyPairConversions._
import play.api.libs.iteratee.Enumerator

/**
 * Displays market spreads.
 *
 * @param spreadQueryActor API actor
 */
class MarketSpreadsController(
                               spreadQueryActor: ActorRef,
                               exchangeApiActor: ActorRef)
  extends Controller
  with Secured {

  implicit val timeout = Timeout(15, TimeUnit.SECONDS)


  def index = asyncWithAuth { user => request =>
    val responseFuture = spreadQueryActor ? MarketSpreadQuery(DateTime.now - 30.days, DateTime.now, None)
    responseFuture map {
      case response: MarketSpreadsResponse =>
        Ok(views.html.spreads(response.spreads))
      case _ =>
        InternalServerError("Something went wrong")
    }
  }


  def exchanges = asyncWithAuth { user => request =>
    val currencyPairsFuture = (exchangeApiActor ? CurrencyPairsRequest).map(_.asInstanceOf[List[CurrencyPair]])
    val exchangesFuture = (exchangeApiActor ? ExchangeListRequest).map(_.asInstanceOf[List[Exchange]])
    val responseFuture = spreadQueryActor ? MarketSpreadQuery(DateTime.now - 30.days, DateTime.now, None)
    responseFuture flatMap {
      case response: MarketSpreadsResponse =>
        for(exchanges <- exchangesFuture;
            pairs <- currencyPairsFuture)
            yield Ok(views.html.exchanges(response.spreads, exchanges, pairs))
      case _ =>
        //I know it looks weird, but it's the Response that matters, not the Future status
        Future.successful(InternalServerError("Something went wrong"))
    }
  }


  def spreadsForCurrencyPair(primary: String, secondary: String) = asyncWithAuth { user => request =>
    val currencyPair: CurrencyPair = primary -> secondary
    val exchangesFuture = (exchangeApiActor ? ExchangeListRequest).map(_.asInstanceOf[List[Exchange]])
    val responseFuture = spreadQueryActor ? MarketSpreadQuery(DateTime.now - 30.days, DateTime.now, None)
    responseFuture flatMap {
      case response: MarketSpreadsResponse =>
        for(exchanges <- exchangesFuture)
        yield Ok(views.html.currencyPairs(currencyPair))
      case _ =>
        //I know it looks weird, but it's the Response that matters, not the Future status
        Future.successful(InternalServerError("Something went wrong"))
    }
  }

  def allSpreads = asyncWithAuth { user => request =>
    def spreadToCSV(spread: MarketSpread): Array[Byte] = {
      (s"""
        |${spread.market1.currencyPair.primary},
        |${spread.market1.currencyPair.secondary},
        |${spread.start.toString("yyyy-MM-dd HH:mm:ss")},
        |${spread.end.fold("-")(_.toString("yyyy-MM-dd HH:mm:ss"))},
        |${spread.spread},
        |${spread.market1.exchange},
        |${spread.market1.ask.map(_.price).getOrElse(0)},
        |${spread.market1.ask.map(_.volume).getOrElse(0)},
        |${spread.market1.bid.map(_.price).getOrElse(0)},
        |${spread.market1.ask.map(_.volume).getOrElse(0)},
        |${spread.market2.exchange},
        |${spread.market2.ask.map(_.price).getOrElse(0)},
        |${spread.market2.ask.map(_.volume).getOrElse(0)},
        |${spread.market2.bid.map(_.price).getOrElse(0)},
        |${spread.market2.ask.map(_.volume).getOrElse(0)},
      """.stripMargin.replaceAllLiterally("\n", "") + "\n").getBytes
    }
    val enumeratorFuture = (spreadQueryActor ? AllRecords).map(_.asInstanceOf[Enumerator[MarketSpread]])
    val result = enumeratorFuture.map( enumerator =>
      Enumerator(
        ("""
          |Primary,
          |Secondary,
          |Start,
          |End,
          |Spread,
          |Exchange 1,
          |Ask price Ex 1,
          |Ask volume Ex 1,
          |Bid price Ex 1,
          |Bid volume Ex 1,
          |Exchange 2,
          |Ask price Ex 2,
          |Ask volume Ex 2,
          |Bid price Ex 2,
          |Bid volume Ex 2
        """.stripMargin.replaceAllLiterally("\n", "") + "\n").getBytes) >>> enumerator.map(spreadToCSV)
    )
    result.map(Ok.chunked(_).as(withCharset("text/csv")))
  }
}
