package controllers

import play.api.mvc.{Action, Controller}
import akka.pattern.ask
import actors.ExchangeAPI.{OrderListResponse, OrdersListRequest}
import akka.util.Timeout
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.Future
import akka.actor.ActorRef

/**
 * Actions related to account management
 */
class TradesController(exchangeApi: ActorRef) extends Controller {

  implicit val timeout = Timeout(10 seconds)

  def index = Action.async { request =>
    (exchangeApi ? OrdersListRequest).map {
      case OrderListResponse(exchange, trades) =>
        Ok(views.html.trades(trades))
    }.recover {
      case _ =>
        InternalServerError("Couldn't get trades information")
    }
  }

  def cancel(id: String) = Action.async { request =>
    Future.successful(Ok("not implemented"))
  }

}
