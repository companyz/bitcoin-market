arbitrageApp = angular.module('spreads', [])

arbitrageApp.controller('SpreadsController', ($scope, $http) ->

  Array::uniqueCurrencies = ->
    output = {}
    output[@[key].primaryCurrency + '/' + @[key].secondaryCurrency] = @[key] for key in [0...@length]
    value for key, value of output

  startWS = ->
    wsUrl = jsRoutes.controllers.ArbitrageController.websocket().webSocketURL()
    $scope.marketSpreads = {}
    $scope.currencyPairs = []

    initSpreads = (exchange1, exchange2, primary) ->
      if $scope.marketSpreads[exchange1] == undefined
        $scope.marketSpreads[exchange1] = {}
      if $scope.marketSpreads[exchange2] == undefined
        $scope.marketSpreads[exchange2] = {}
      if $scope.marketSpreads[exchange1][primary] == undefined
        $scope.marketSpreads[exchange1][primary] = {}
      if $scope.marketSpreads[exchange2][primary] == undefined
        $scope.marketSpreads[exchange2][primary] = {}


    $scope.socket = new WebSocket(wsUrl)
    $scope.socket.onmessage = (msg) ->
      $scope.$apply( ->
        console.log "received : #{msg.data}"
        data = JSON.parse(msg.data)
        primary = data.market1.primaryCurrency
        secondary = data.market1.secondaryCurrency
        exchange1 = data.market1.exchange
        exchange2 = data.market2.exchange
        initSpreads(exchange1, exchange2, primary)
        $scope.marketSpreads[exchange1][primary][secondary] = data.spread
        $scope.marketSpreads[exchange2][primary][secondary] = data.spread

        pair =
          primaryCurrency: primary,
          secondaryCurrency: secondary
        $scope.currencyPairs.push(pair)
        $scope.currencyPairs = $scope.currencyPairs.uniqueCurrencies()
        $scope.currencyPairs.sort((left,right) ->
          if left.primaryCurrency > right.primaryCurrency
            1
          else if left.primaryCurrency < right.primaryCurrency
            -1
          else if left.secondaryCurrency > right.secondaryCurrency
            1
          else if left.secondaryCurrency < right.secondaryCurrency
            -1
          else
            0

        )
      )

  startWS()

) 
