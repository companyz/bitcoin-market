arbitrageApp = angular.module('arbitrage', [])

arbitrageApp.controller('ArbitrageController', ($scope, $http) ->

  startWS = ->
    wsUrl = jsRoutes.controllers.ArbitrageController.websocket().webSocketURL()
    $scope.marketSpreads = []
  
    $scope.socket = new WebSocket(wsUrl)
    $scope.socket.onmessage = (msg) ->
      $scope.$apply( ->
        console.log "received : #{msg.data}"
        $scope.marketSpreads.push JSON.parse(msg.data)
      )

  startWS()

) 
