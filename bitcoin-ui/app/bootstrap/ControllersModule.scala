package bootstrap

import controllers.{AppController, TradesController, ArbitrageController, MarketSpreadsController}
import akka.actor.ActorRef

class ControllersModule(akkaModule: AkkaModule, dataModule: DataModule) {

  lazy val marketSpreadController = new MarketSpreadsController(
    akkaModule.marketSpreadQueryingActor,
    akkaModule.exchangeApiActor)

  lazy val arbitrageController = new ArbitrageController(akkaModule.ordersPublisherActor)

  lazy val tradesController = new TradesController(akkaModule.cryptsyExchange)

  lazy val appController = new AppController(dataModule.userRecord)

}
