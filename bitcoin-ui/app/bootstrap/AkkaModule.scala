package bootstrap

import actors.cryptsy.CryptsyExchangeActor
import actors.bter.BterExchangeActor
import actors.btce.BtceExchangeActor
import actors.{ExchangeApiActor, OrdersPublisherActor, ArbitrageActor}
import actors.storage.{MarketSpreadQueryingActor, StorageApiActor}
import play.api.Logger
import com.typesafe.config.Config
import akka.actor.ActorSystem
import actors.mint.MintExchangeActor

/**
 * Akka initialization module.
 *
 * @param config system configuration
 * @param system Actor system
 * @param dataModule DAO objects
 */
class AkkaModule(config: Config, system: ActorSystem, dataModule: DataModule) {

  val storageApi = system.actorOf(StorageApiActor.props(dataModule.marketSpreadRecord), "persistence")

  val cryptsyExchange = system.actorOf(CryptsyExchangeActor.props(config), "cryptsy-exchange")

  val bterExchange = system.actorOf(BterExchangeActor.props(config), "bter-exchange")

  val btceExchange = system.actorOf(BtceExchangeActor.props(config), "btce-exchange")

  val mintExchange = system.actorOf(MintExchangeActor.props(config), "mint-exchange")

  val arbitrageActor = system.actorOf(ArbitrageActor.props(Map(
    CryptsyExchangeActor.exchange -> cryptsyExchange,
    BterExchangeActor.exchange -> bterExchange,
    BtceExchangeActor.exchange -> btceExchange,
    MintExchangeActor.exchange -> mintExchange
  )), "arbitrage")

  lazy val ordersPublisherActor = system.actorOf(OrdersPublisherActor.props(arbitrageActor), "publishing")

  lazy val marketSpreadQueryingActor = system.actorOf(MarketSpreadQueryingActor.props(dataModule.marketSpreadRecord), "storage")

  val exchangeApiActor = system.actorOf(ExchangeApiActor.props(Map(
    CryptsyExchangeActor.exchange -> cryptsyExchange,
    BterExchangeActor.exchange -> bterExchange,
    BtceExchangeActor.exchange -> btceExchange,
    MintExchangeActor.exchange -> mintExchange
  )))

  Logger.info("Actors initialized")

}
