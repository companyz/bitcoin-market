import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import scala.concurrent.Future
import play.libs.Akka
import bootstrap.{DataModule, ControllersModule, AkkaModule}
import com.softwaremill.macwire.{InstanceLookup, Macwire}

object Global extends GlobalSettings with Macwire {

  var akkaModule: AkkaModule = _

  var controllersLookup: InstanceLookup = _

  override def onStart(app: Application) {
    val config = app.configuration.underlying
    val dataModule = new DataModule(config)
    val akkaModule = new AkkaModule(config, Akka.system(), dataModule)
    val controllersModule = new ControllersModule(akkaModule, dataModule)
    controllersLookup = InstanceLookup(valsByClass(controllersModule))
  }

  override def getControllerInstance[A](controllerClass: Class[A]): A = {
    controllersLookup.lookupSingleOrThrow(controllerClass)
  }

  override def onHandlerNotFound(request: RequestHeader) = {
    Future.successful(
      Redirect(controllers.routes.AppController.index()))
  }

  override def onStop(app: Application) = {
    Akka.system.shutdown()
  }

}
