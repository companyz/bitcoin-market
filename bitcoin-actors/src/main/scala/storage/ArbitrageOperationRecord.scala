package storage

import java.util.UUID
import com.datastax.driver.core.{ResultSet, Session, Row}
import com.newzly.phantom.Implicits._
import models._
import models.ArbitrageOperation
import models.OrderRequest
import models.CurrencyPair
import org.joda.time.DateTime
import com.newzly.phantom.query.QueryCondition
import com.datastax.driver.core.querybuilder.QueryBuilder
import scala.concurrent.Future
import scala.annotation.tailrec
import scala.collection.immutable.Seq

sealed class ArbitrageOperationRecord private()
  extends CassandraTable[ArbitrageOperationRecord, ArbitrageOperation]  {

  object id extends UUIDColumn(this) with PartitionKey[UUID]
  object month extends StringColumn(this) with Index[String]
  object created extends DateTimeColumn(this) with Index[DateTime]
  object sellExchange extends StringColumn(this) with Index[String]
  object sellPrimaryCurrency extends StringColumn(this) with Index[String]
  object sellSecondaryCurrency extends StringColumn(this) with Index[String]
  object sellAmount extends BigDecimalColumn(this)
  object sellPrice extends BigDecimalColumn(this)
  object buyExchange extends StringColumn(this) with Index[String]
  object buyPrimaryCurrency extends StringColumn(this)
  object buySecondaryCurrency extends StringColumn(this)
  object buyAmount extends BigDecimalColumn(this)
  object buyPrice extends BigDecimalColumn(this)

  override def fromRow(row: Row): ArbitrageOperation = {
    ArbitrageOperation(
      OrderRequest(
        Sell,
        sellExchange(row),
        CurrencyPair(sellPrimaryCurrency(row), sellSecondaryCurrency(row)),
        sellAmount(row),
        sellPrice(row)
      ),
      OrderRequest(
        Buy,
        buyExchange(row),
        CurrencyPair(buyPrimaryCurrency(row), buySecondaryCurrency(row)),
        buyAmount(row),
        buyPrice(row)
      ),
      created(row)
    )
  }
}

object ArbitrageOperationRecord extends ArbitrageOperationRecord {

  override val tableName = "arbitrage"

  def storeRecord(operation: ArbitrageOperation)(implicit session: Session): Future[ResultSet] = {
    logger.info("Inserting new marketSpread")
    val insertQuery = insert
      .value(_.id, UUID.randomUUID())
      .value(_.month, operation.created.toString("yyyyMM"))
      .value(_.created, operation.created)
      .value(_.buyExchange, operation.buyOrder.exchange)
      .value(_.buyPrimaryCurrency, operation.buyOrder.pair.primary)
      .value(_.buySecondaryCurrency, operation.buyOrder.pair.secondary)
      .value(_.buyAmount, operation.buyOrder.amount)
      .value(_.buyPrice, operation.buyOrder.price)
      .value(_.sellExchange, operation.sellOrder.exchange)
      .value(_.sellPrimaryCurrency, operation.sellOrder.pair.primary)
      .value(_.sellSecondaryCurrency, operation.sellOrder.pair.secondary)
      .value(_.sellAmount, operation.sellOrder.amount)
      .value(_.sellPrice, operation.sellOrder.price)
    insertQuery.future()
  }

  def recordsInRange(from: DateTime, to: DateTime)(implicit session: Session): Future[Seq[ArbitrageOperation]] = {

    @tailrec
    def intervalToMonths(start: DateTime, end: DateTime, months: List[String]): List[String] = {
      if(start.monthOfYear().roundFloorCopy().isAfter(end)) months
      else intervalToMonths(start.plusMonths(1), end, months :+ start.toString("yyyyMM"))
    }

    logger.info(s"querying records between $from and $to")
    val conditionTo = QueryCondition(
      QueryBuilder.lte("created", to.toDate)
    )
    val conditionFrom = QueryCondition(
      QueryBuilder.gte("created", from.toDate)
    )
    val months = intervalToMonths(from, to, Nil)

    val partialFutures = months.map( month =>
      ArbitrageOperationRecord.select.allowFiltering().where(_.month eqs month).and(_=>conditionFrom).and(_=>conditionTo).fetch()
    )
    val futureList = Future.sequence(partialFutures)
    futureList.map(_.flatten)
  }

}