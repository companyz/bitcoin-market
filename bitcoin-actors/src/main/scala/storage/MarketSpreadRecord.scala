package storage

import com.datastax.driver.core.{ResultSet, Row}
import com.newzly.phantom.Implicits._
import models.MarketSpread
import org.joda.time.DateTime
import scala.concurrent.Future
import scala.annotation.tailrec
import scala.collection.immutable.Seq
import actors.ExchangeAPI.MarketInfo
import scala.Some
import actors.ExchangeAPI.ExchangeOrder
import models.CurrencyPair
import com.newzly.phantom.column.AbstractColumn
import play.api.libs.iteratee.{Iteratee, Enumerator}

sealed class MarketSpreadRecord(client: CassandraClient)
  extends CassandraTable[MarketSpreadRecord, MarketSpread]  {

  override val tableName = "spreads"

  implicit lazy val session = client.session

  object month extends StringColumn(this) with PartitionKey[String]
  object start extends DateTimeColumn(this) with PrimaryKey[DateTime]
  object end extends DateTimeColumn(this)
  object primaryCurrency extends StringColumn(this) with Index[String]
  object secondaryCurrency extends StringColumn(this) with Index[String]
  object exchange1 extends StringColumn(this) with Index[String]
  object ask1Price extends BigDecimalColumn(this)
  object ask1Volume extends BigDecimalColumn(this)
  object bid1Price extends BigDecimalColumn(this)
  object bid1Volume extends BigDecimalColumn(this)
  object exchange2 extends StringColumn(this) with Index[String]
  object ask2Price extends BigDecimalColumn(this)
  object ask2Volume extends BigDecimalColumn(this)
  object bid2Price extends BigDecimalColumn(this)
  object bid2Volume extends BigDecimalColumn(this)
  object spread extends BigDecimalColumn(this)

  override def fromRow(row: Row): MarketSpread = {
    MarketSpread(
      MarketInfo(
        CurrencyPair(primaryCurrency(row), secondaryCurrency(row)),
        exchange1(row),
        Some(ExchangeOrder(ask1Price(row), ask1Volume(row))),
        Some(ExchangeOrder(bid1Price(row), bid1Volume(row)))
      ),
      MarketInfo(
        CurrencyPair(primaryCurrency(row), secondaryCurrency(row)),
        exchange2(row),
        Some(ExchangeOrder(ask2Price(row), ask2Volume(row))),
        Some(ExchangeOrder(bid2Price(row), bid2Volume(row)))
      ),
      spread(row),
      start(row),
      Some(end(row))
    )
  }

  def persist(marketSpread: MarketSpread): Future[ResultSet] = {
    marketSpread.end match {
      case Some(end) =>
        logger.info(s"Inserting new marketSpread ${marketSpread.exchanges} ${marketSpread.market1.currencyPair}")
        val insertQuery = insert
          .value(_.month, marketSpread.start.toString("yyyyMM"))
          .value(_.start, marketSpread.start)
          .value(_.end, end)
          .value(_.primaryCurrency, marketSpread.market1.currencyPair.primary)
          .value(_.secondaryCurrency, marketSpread.market1.currencyPair.secondary)
          .value(_.exchange1, marketSpread.market1.exchange)
          .value(_.ask1Price, marketSpread.market1.ask.map(_.price).get)
          .value(_.ask1Volume, marketSpread.market1.ask.map(_.volume).get)
          .value(_.bid1Price, marketSpread.market1.bid.map(_.price).get)
          .value(_.bid1Volume, marketSpread.market1.bid.map(_.volume).get)
          .value(_.exchange2, marketSpread.market2.exchange)
          .value(_.ask2Price, marketSpread.market2.ask.map(_.price).get)
          .value(_.ask2Volume, marketSpread.market2.ask.map(_.volume).get)
          .value(_.bid2Price, marketSpread.market2.bid.map(_.price).get)
          .value(_.bid2Volume, marketSpread.market2.bid.map(_.volume).get)
          .value(_.spread, marketSpread.spread)
        insertQuery.future()
      case None =>
        Future.failed(throw new IllegalArgumentException("Spread has no end time information"))
    }
  }

  def recordsInRange(from: DateTime, to: DateTime, limit: Int = 100): Future[Seq[MarketSpread]] = {

    @tailrec
    def intervalToMonths(start: DateTime, end: DateTime, months: List[String]): List[String] = {
      if(start.monthOfYear().roundFloorCopy().isAfter(end)) months
      else intervalToMonths(start.plusMonths(1), end, months :+ start.toString("yyyyMM"))
    }

    logger.info(s"querying records between $from and $to")
    val months = intervalToMonths(from, to, Nil)

    val partialFutures = months.map { month =>
        val selectQuery = select.allowFiltering().
          where(_.month eqs month).
          and(_.start gte from).
          and(_.start lte to)
        logger.debug(s"Executing query $selectQuery")
        selectQuery.limit(limit).fetch()
    }
    val futureList = Future.sequence(partialFutures)
    futureList.map(_.flatten)
  }


  def allRecords: Enumerator[MarketSpread] = {
    select.fetchEnumerator
  }

}
