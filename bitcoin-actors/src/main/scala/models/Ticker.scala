package models

import org.joda.time.DateTime

/**
 * Ticker representation
 */
case class Ticker(currencyPair: CurrencyPair,
                  exchange: String,
                  last: BigDecimal,
                  buy: Option[BigDecimal],
                  sell: Option[BigDecimal],
                  volume: Option[BigDecimal],
                  time: DateTime)

object Ticker {

  def apply(currencyPair: CurrencyPair,
    exchange: String,
    last: BigDecimal,
    buy: BigDecimal,
    sell: BigDecimal,
    volume: BigDecimal,
    time: DateTime): Ticker = Ticker(currencyPair, exchange, last, Some(buy), Some(sell), Some(volume), time)

}
