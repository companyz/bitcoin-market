package models

/**
 * Authenticated user
 */
case class User(username:String, password:String, seed: String) {

  def checkPassword(password: String): Boolean = this.password == password

}
