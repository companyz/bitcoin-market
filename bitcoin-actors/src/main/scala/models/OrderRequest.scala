package models

sealed trait OrderType
case object Buy extends OrderType
case object Sell extends OrderType

/**
 * Order information.
 */
case class OrderRequest(
                  orderType:OrderType,
                  exchange: String,
                  pair: CurrencyPair,
                  amount: BigDecimal,
                  price: BigDecimal)
