package models

import actors.ExchangeAPI.MarketInfo
import org.joda.time.DateTime


/**
 * Value of spread between two exchanges for given currency
 */
case class MarketSpread(market1: MarketInfo,
                        market2: MarketInfo,
                        spread: BigDecimal,
                        start: DateTime = DateTime.now,
                        end: Option[DateTime] = None) {

  /**
   * Alphabetically sorted tuple of exchanges
   */
  lazy val exchanges = {
    if(market1.exchange < market2.exchange) {
      market1.exchange -> market2.exchange
    } else {
      market2.exchange -> market1.exchange
    }
  }

  val actionable =
    spread != BigDecimal(0) &&
      market1.bid.isDefined && market1.ask.isDefined &&
      market2.bid.isDefined && market2.ask.isDefined &&
      market1.bid.get.price < market1.ask.get.price &&
      market2.bid.get.price < market2.ask.get.price

}

/**
 * Builds the spread from recognized market change requests
 */
object MarketSpread {
  def from(info1: (MarketInfo, BigDecimal), info2: (MarketInfo, BigDecimal)): MarketSpread = {
    val (left, right) =
      if(info1._1.exchange < info2._1.exchange) (info1, info2)
      else (info2, info1)

    val (market1, fee1) = left
    val (market2, fee2) = right
    if( market1.bid.isDefined && market2.ask.isDefined &&
      market1.bid.get.price * (1 - fee1 / 100.0) > market2.ask.get.price * (1 + fee2 / 100.0)) {
      val spreadNet = market1.bid.get.price * (1 - fee1 / 100.0) - market2.ask.get.price * (1 + fee2 / 100.0)
      MarketSpread(market1, market2, spreadNet)
    } else if( market1.bid.isDefined && market2.ask.isDefined &&
      market2.bid.get.price * (1 - fee2 / 100.0) > market1.ask.get.price * (1 + fee1 / 100.0)) {
      val spreadNet = market1.ask.get.price * (1 + fee1 / 100.0) - market2.bid.get.price * (1 - fee2 / 100.0)
      MarketSpread(market1, market2, spreadNet)
    } else {
      MarketSpread(market1, market2, 0)
    }
  }
}