package actors.storage

import akka.actor.{ActorLogging, Actor, Props}
import org.joda.time.DateTime
import models.{MarketSpread, CurrencyPair}
import actors.storage.MarketSpreadQueryingActor.{AllRecords, MarketSpreadsResponse, MarketSpreadQuery}
import akka.pattern.pipe
import storage.MarketSpreadRecord
import scala.concurrent.ExecutionContext.Implicits.global

object MarketSpreadQueryingActor {

  def props(marketSpreadRecord: MarketSpreadRecord) = Props(classOf[MarketSpreadQueryingActor], marketSpreadRecord)

  case class MarketSpreadQuery(
                                dateFrom: DateTime,
                                dateTo: DateTime,
                                pair: Option[CurrencyPair],
                                page: Int = 1,
                                limit: Int = 100)

  case class MarketSpreadsResponse(spreads: List[MarketSpread])
  case object AllRecords

}

class MarketSpreadQueryingActor(marketSpreadRecord: MarketSpreadRecord)
  extends Actor
  with ActorLogging {

  override def receive: Receive = {
    case query: MarketSpreadQuery =>
      log.debug(s"Market spread query $query")

      marketSpreadRecord.recordsInRange(query.dateFrom, query.dateTo) map { result =>
        MarketSpreadsResponse(result.toList)
      } pipeTo sender
    case AllRecords =>
      log.debug(s"All records request")
      sender ! marketSpreadRecord.allRecords
  }


}
