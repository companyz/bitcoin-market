package actors.storage

import akka.actor.{Props, ActorLogging, Actor}
import models.MarketSpread
import storage.MarketSpreadRecord
import actors.SpreadDeduplicationActor.MarketSpreadWindowClosed


object MarketSpreadPersistingActor {

  def props(marketSpreadRecord: MarketSpreadRecord) = Props(classOf[MarketSpreadPersistingActor], marketSpreadRecord)

}

/**
 * Processes ArbitrageOperation events and stores them to a persistent storage.
 * The actor also observes for how long given pair had a positive spread. 
 */
class MarketSpreadPersistingActor(marketSpreadRecord: MarketSpreadRecord)
  extends Actor
  with ActorLogging {

  override def preStart() = {
    log.info("Subscribing form Market spread changes")
    context.system.eventStream.subscribe(self, classOf[MarketSpreadWindowClosed])
  }

  override def receive: Receive = {
    case MarketSpreadWindowClosed(spread) if spread.end.isDefined =>
      log.info(s"Market spread arrived $spread")
      marketSpreadRecord.persist(spread)
  }

}
