package actors.storage

import akka.actor.{Props, ActorRef, Actor, ActorLogging}
import actors.storage.MarketSpreadQueryingActor.MarketSpreadQuery
import com.datastax.driver.core.Session
import storage.MarketSpreadRecord
import models.MarketSpread


object StorageApiActor {

  def props(marketSpreadRecord: MarketSpreadRecord) = Props(classOf[StorageApiActor], marketSpreadRecord)

}

class StorageApiActor(marketSpreadRecord: MarketSpreadRecord)
  extends Actor
  with ActorLogging {

  var queryActor: ActorRef = _

  var storeActor: ActorRef = _


  override def preStart() = {
    log.info("Initiating Storage API")
    queryActor = context.system.actorOf(MarketSpreadQueryingActor.props(marketSpreadRecord), "market-spread-query")
    storeActor = context.system.actorOf(MarketSpreadPersistingActor.props(marketSpreadRecord), "market-spread-persist")
  }

  override def receive: Receive = {
    case q: MarketSpreadQuery =>
      queryActor forward q
    case spread: MarketSpread =>
      storeActor forward spread
  }

}
