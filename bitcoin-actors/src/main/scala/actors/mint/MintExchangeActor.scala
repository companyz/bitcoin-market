package actors.mint

import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import akka.actor.Actor.Receive
import actors.ExchangeAPI.Exchange
import actors.{FixedFeeExchange, ExchangeBaseActor}
import com.typesafe.config.Config


object MintExchangeActor {

  def props(config: Config) = Props(classOf[MintExchangeActor], config)

  def exchange: Exchange = "Mintpal"

}

/**
 * Mintpal exchange.
 */
class MintExchangeActor(config: Config)
  extends ExchangeBaseActor
  with FixedFeeExchange
  with ActorLogging {

  import context._

  override val fee: BigDecimal = config.getDouble("mintpal.fee")

  override val depthActor: ActorRef = system.actorOf(MintDepthActor.props)

  override val feeActor: ActorRef = null.asInstanceOf[ActorRef]

  override val openOrdersActor: ActorRef = null.asInstanceOf[ActorRef]

  override val currencyPairsActor: ActorRef = system.actorOf(TradingPairsActor.props, "currency-pairs")
}
