package actors.mint

import akka.actor.{Props, ActorLogging, Actor}
import spray.httpx.unmarshalling._
import akka.pattern.pipe
import spray.json._
import models._
import scala.concurrent.Future
import scala.concurrent.duration._
import spray.client.pipelining._
import spray.http.{HttpEntity, HttpResponse, HttpRequest}
import scala.Some
import actors.ExchangeAPI.{ErrorResponse, MarketInfo, ExchangeOrder, MarketInfoRequest}
import models.CurrencyPair
import akka.util.Timeout
import scala.concurrent.ExecutionContext.Implicits.global

object MintDepthActor {

  def props = Props(classOf[MintDepthActor])
}

/**
 * Collects current market depth
 */
class MintDepthActor
  extends Actor
  with ActorLogging {

  implicit val timeout = Timeout(10 seconds)

  def apiUrl(currencyPair: CurrencyPair, orderType: OrderType) =
    s"https://api.mintpal.com/v1/market/orders/${currencyPair.primary}/${currencyPair.secondary}/$orderType"

  private val buyUnMarshaller = new TopOfferUnmarshaller(Buy)
  private val sellUnMarshaller = new TopOfferUnmarshaller(Sell)

  private val pipeline: HttpRequest => Future[HttpResponse] = sendReceive

  private def collectBuyOrder(pair: CurrencyPair): Future[Deserialized[Option[ExchangeOrder]]] = {
    pipeline(Get(apiUrl(pair, Buy))) map { response =>
      response.entity.as(buyUnMarshaller)
    }
  }

  private def collectSellOrder(pair: CurrencyPair): Future[Deserialized[Option[ExchangeOrder]]] = {
    pipeline(Get(apiUrl(pair, Sell))) map { response =>
      response.entity.as(sellUnMarshaller)
    }
  }

  override def receive: Receive = {
    case req @ MarketInfoRequest(pair, _) =>
      log.info(s"Collecting offers for $pair")
      val result = for( sellOrder <- collectSellOrder(pair); buyOrder <- collectBuyOrder(pair)) yield {
        if(sellOrder.isLeft) {
          ErrorResponse(req, sellOrder.left.get.toString)
        } else if(buyOrder.isLeft) {
          ErrorResponse(req, buyOrder.left.get.toString)
        } else {
          MarketInfo(pair, MintExchangeActor.exchange, sellOrder.right.get, buyOrder.right.get)
        }
      }
      result pipeTo sender
  }


  class TopOfferUnmarshaller(operation: OrderType)
    extends Unmarshaller[Option[ExchangeOrder]] with JsonReader[Option[ExchangeOrder]] {

    import actors.JsonExtensions._

    /**
     * Compares two objects and returns either on them - with bigger price for Buy, with smaller price for Sell.
     */
    def compare(l: JsObject, r: JsObject): JsObject = {
      val lPrice = l.fields("price").toBigDecimal
      val rPrice = r.fields("price").toBigDecimal
      if(operation == Sell) {
        if(lPrice < rPrice) l
        else if(lPrice == rPrice) l
        else r
      } else {
        if(lPrice > rPrice) l
        else if(lPrice == rPrice) l
        else r
      }
    }

    override def read(json: JsValue): Option[ExchangeOrder] = {
      val ordersJs = json.asJsObject.fields("orders")
      ordersJs match {
        case ordersArray: JsArray =>
          val topOrder = ordersArray.elements.foldLeft(None: Option[JsObject]) {(l,r) =>
            val rObj = r.asJsObject()
            l match {
              case Some(obj: JsObject) =>
                Some(compare(obj, rObj))
              case None =>
                Some(rObj)
            }
          }
          topOrder.map { jsObj =>
            ExchangeOrder(jsObj.fields("price").toBigDecimal, jsObj.fields("amount").toBigDecimal)
          }
        case _ =>
          throw new IllegalArgumentException("Invalid json")
      }
    }

    override def apply(entity: HttpEntity): Deserialized[Option[ExchangeOrder]] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[Option[ExchangeOrder]](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }
  }

}
