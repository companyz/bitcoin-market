package actors

import akka.actor._
import models.MarketSpread
import actors.SpreadDeduplicationActor._
import models.CurrencyPair
import actors.ExchangeAPI.Exchange
import org.joda.time.DateTime

object SpreadDeduplicationActor {

  def props(currencyPair: CurrencyPair, exchange1: Exchange, exchange2: Exchange) =
    Props(classOf[SpreadDeduplicationActor], currencyPair, exchange1, exchange2)

  sealed trait State
  case object Open extends State
  case object Closed extends State

  sealed trait MarketSpreadWindow
  case class MarketSpreadWindowOpen(spread: MarketSpread) extends MarketSpreadWindow
  case class MarketSpreadWindowClosed(spread: MarketSpread) extends MarketSpreadWindow
  case object Uninitialized extends MarketSpreadWindow
}

/**
 * Deduplicates MarketSpread events.
 */
class SpreadDeduplicationActor(currencyPair: CurrencyPair, exchange1: Exchange, exchange2: Exchange)
  extends Actor
  with FSM[State, MarketSpreadWindow]
  with ActorLogging {

  import context._

  startWith(Closed, Uninitialized)

  when(Closed) {
    case Event(spread: MarketSpread, _) if spread.spread > 0 && spread.actionable =>
      log.info(s"Opening spread window for $currencyPair between $exchange1 and $exchange2")
      goto(Open) using MarketSpreadWindowOpen(spread)
    case Event(spread: MarketSpread, _) =>
      log.info(s"Ignoring opening zero or not actionable value spread")
      stay using Uninitialized
  }

  when(Open) {
    case Event(spread: MarketSpread, window: MarketSpreadWindowOpen) if spread.spread > 0 && spread.actionable =>
      log.info(s"Spread positive for $currencyPair between $exchange1 and $exchange2, staying in open state")
      stay using MarketSpreadWindowOpen(spread)
    case Event(spread: MarketSpread, window: MarketSpreadWindowOpen) =>
      log.info(s"Closing spread window for $currencyPair between $exchange1 and $exchange2")
      goto(Closed) using MarketSpreadWindowClosed(spread = window.spread.copy(end = Some(DateTime.now)))
  }

  onTransition {
    case Open -> Closed =>
      nextStateData match {
        case window: MarketSpreadWindowClosed =>
          log.info(s"Closed spread window for $currencyPair between $exchange1 and $exchange2")
          system.eventStream.publish(window)
        case _ =>
          throw new IllegalStateException(s"Invalid data in Closed state $stateData")
      }
    case Closed -> Open =>
      nextStateData match {
        case window: MarketSpreadWindowOpen =>
          log.info(s"Opened spread window for $currencyPair between $exchange1 and $exchange2")
          system.eventStream.publish(window)
      }
  }

}
