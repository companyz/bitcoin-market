package actors

import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import actors.ExchangeAPI.{CurrencyPairsResponse, Exchange, CurrencyPairsRequest}
import actors.ExchangeApiActor.ExchangeListRequest
import akka.pattern.{ ask, pipe }
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Future
import scala.collection.immutable.Seq
import models.CurrencyPair
import scala.concurrent.ExecutionContext.Implicits.global

object ExchangeApiActor {

  def props(exchangeActors: Map[Exchange, ActorRef]) = Props(classOf[ExchangeApiActor], exchangeActors)

  case object ExchangeListRequest

}


/**
 * Aggregates all exchanges delivering aggregating API
 */
class ExchangeApiActor(exchangeActors: Map[Exchange, ActorRef])
  extends Actor
  with ActorLogging {

  implicit val timeout = Timeout(10 seconds)

  override def receive: Receive = {
    case msg @ CurrencyPairsRequest =>
      log.debug("Currency pairs request")
      val results = Future.sequence(exchangeActors.values.map { exchg =>
        exchg ? CurrencyPairsRequest
      })
      val pairsFuture = results.map(_.map(_.asInstanceOf[CurrencyPairsResponse].pairs).flatten.toList.sortBy(_.primary))
      val uniquePairs = pairsFuture.map(_.distinct)
      uniquePairs pipeTo sender
    case ExchangeListRequest =>
      sender ! exchangeActors.map {
        case (exchange, _) => exchange
      }.toList.sorted
  }

}
