package actors.btce

import akka.actor.{Props, ActorLogging, Actor}
import com.typesafe.config.Config
import actors.ExchangeAPI.OrdersListRequest
import scala.concurrent.Future
import models.{Sell, Buy}
import akka.pattern.pipe
import spray.client.pipelining._
import spray.http.HttpEntity
import spray.httpx.unmarshalling._
import spray.json._
import actors.ApiException
import actors.bter.BterExchangeActor
import common.AuthenticatedRequestBuilder
import org.joda.time.DateTime
import spray.http.HttpRequest
import actors.ExchangeAPI.OrderListResponse
import models.Order
import scala.concurrent.ExecutionContext.Implicits.global

object OrderListActor {

  def props(config: Config) = Props(classOf[OrderListActor], config)

}


class OrderListActor(config: Config)
  extends Actor
  with ActorLogging {

  implicit val unmarshaller = new OrderListUnmarshaller

  val pipeline: HttpRequest => Future[List[Order]] = sendReceive ~> unmarshal[List[Order]]

  val url = "https://btc-e.com/tapi"

  val method = "ActiveOrders"

  val publicKey = config.getString("btce.public-key")

  val privateKey = config.getString("btce.private-key")

  override def receive: Receive = {
    case OrdersListRequest =>
      log.info("Handling order list request")
      val ordersFuture: Future[List[Order]] = collectOrders
      val responseFuture = ordersFuture map { orders =>
        log.debug("Order list response ready do be sent")
        OrderListResponse(BtceExchangeActor.exchange, orders)
      }
      responseFuture pipeTo sender
  }

  def collectOrders: Future[List[Order]] = {
    val requestBuilder = new AuthenticatedRequestBuilder(url, publicKey, privateKey)
    val request = requestBuilder.buildRequest("method" -> method)
    log.debug(s"Sending $request")
    val ordersFuture = pipeline(request)
    ordersFuture
  }

}

class OrderListUnmarshaller  extends Unmarshaller[List[Order]] with JsonReader[List[Order]] {
  import actors.JsonConversions._
  import actors.JsonExtensions._


  override def read(json: JsValue): List[Order] = {
    val resultJs = json.asJsObject
    val success: Boolean = resultJs.fields("success")
    if (success) {
      val ordersJs: JsObject = resultJs.fields("return")
      val orders = ordersJs.fields.map {
        case (id, value) => parseOrder(id, value)
      }
      orders.toList
    } else if(resultJs.fields("error").stringValue == "no orders") {
      Nil
    } else {
      throw new ApiException(s"Error from BTCE API ${resultJs.fields("error")}")
    }
  }

  def parseOrder(id: String, orderJs: JsValue): Order = {
    import models.CurrencyPairConversions._
    val pairValue: String = orderJs.fields("pair")
    val created: Int = orderJs.fields("timestamp_created")
    Order(
      id = id,
      exchange = BterExchangeActor.exchange,
      pair = pairValue,
      created = new DateTime(created.toLong),
      orderType = orderJs.fields("type").mapString {
        case "buy" => Buy
        case "sell" => Sell
      },
      price = orderJs.fields("rate"),
      quantity = orderJs.fields("amount"),
      originalQuantity = orderJs.fields("amount")
    )
  }

  override def apply(entity: HttpEntity): Deserialized[List[Order]] = {
    val parser = JsonParser(entity.asString)
    try {
      Right(parser.convertTo[List[Order]](this))
    } catch {
      case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
    }
  }
}
