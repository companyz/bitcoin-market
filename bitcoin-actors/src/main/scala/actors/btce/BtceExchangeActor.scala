package actors.btce

import akka.actor.{Props, ActorRef, Actor, ActorLogging}
import actors.ExchangeBaseActor
import com.typesafe.config.Config
import actors.ExchangeAPI.Exchange

object BtceExchangeActor {

  def props(config: Config) = Props(classOf[BtceExchangeActor], config)

  val exchange = "BTCE"
}

/**
 * BTC-E exchange API actor
 */
class BtceExchangeActor(config: Config) extends ExchangeBaseActor {

  import context._

  override val feeActor: ActorRef = actorOf(BtceCalculateFeeActor.props, "fee")

  override val depthActor: ActorRef = actorOf(BtceDepthActor.props, "reference")

  override val currencyPairsActor: ActorRef = feeActor

  override val openOrdersActor = actorOf(OrderListActor.props(config))
}
