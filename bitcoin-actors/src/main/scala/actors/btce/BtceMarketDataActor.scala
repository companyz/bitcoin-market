package actors.btce

import akka.actor.{Actor, ActorLogging, Props}
import models.{CurrencyPair, Ticker}
import spray.httpx.unmarshalling._
import akka.io.IO
import spray.can.Http
import spray.http.{HttpEntity, Uri}
import spray.http.HttpMethods._
import spray.json.{JsonParser, JsValue, JsonReader, JsNumber}
import spray.http.HttpRequest
import org.joda.time.DateTime

object BtceMarketDataActor {

  def props(currencyPair: CurrencyPair): Props = Props(new BtceMarketDataActor(currencyPair))
}

/**
 * Collects tickers from btc-e.com exchange.
 * Ticker API (sample for btc->ltc pair): https://btc-e.com/api/2/btc_ltc/ticker
 * {{{
 *   {
 *    ticker: {
 *      high: 565.99902,
 *      low: 540.09998,
 *      avg: 553.0495,
 *      vol: 4062987.44011,
 *      vol_cur: 7304.12711,
 *      last: 557.802,
 *      buy: 560,
 *      sell: 557.8,
 *      updated: 1393749302,
 *      server_time: 1393749303
 *    }
 *   }
 * }}}
 */
class BtceMarketDataActor(currencyPair: CurrencyPair)
  extends Actor
  with ActorLogging {

  val url = s"https://btc-e.com/api/2/${currencyPair.primary.toLowerCase}_${currencyPair.secondary.toLowerCase}/ticker/"

  import context._

  /**
   * Invoked in given interval by scheduler. The action should request data update from corresponding API.
   */
  def sendUpdateRequest(): Unit = {
    IO(Http) ! HttpRequest(GET, Uri(url))
  }


  class TickerUnmarshaller(currencyPair: CurrencyPair) extends Unmarshaller[Ticker] with JsonReader[Ticker] {

    /**
     * Expected response example:
     * {{{
     *   {
     *    result: "true",
     *    last: "0.00002770",
     *    high: "0.00003021",
     *    low: "0.00002676",
     *    avg: "0.00002873",
     *    sell: "0.00002899",
     *    buy: "0.00002689",
     *    vol_bqc: 38853.21,
     *    vol_btc: 1.116066
     *   }
     * }}}
     *
     * @param json JSON object reference
     * @return Returns Ticker object
     */
    override def read(json: JsValue): Ticker = {
      val ticker = json.asJsObject.fields("ticker").asJsObject
      val last = ticker.fields("last").asInstanceOf[JsNumber].value
      val buy = ticker.fields("buy").asInstanceOf[JsNumber].value
      val sell = ticker.fields("sell").asInstanceOf[JsNumber].value
      val volume = ticker.fields("vol").asInstanceOf[JsNumber].value
      val time = ticker.fields("updated").asInstanceOf[JsNumber].value.toLong
      Ticker(currencyPair, "BTCE", last, buy, sell, volume, new DateTime(time*1000l))
    }

    override def apply(entity: HttpEntity): Deserialized[Ticker] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[Ticker](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }
  }

  implicit val unmarshaller = new TickerUnmarshaller(currencyPair)

  override def receive: Actor.Receive = ???
}
