package actors.btce

import akka.actor._
import spray.http._
import spray.http.HttpMethods._
import spray.httpx.unmarshalling._
import spray.json._
import scala.concurrent.Future
import spray.client.pipelining._
import actors.ExchangeAPI._
import spray.http.HttpRequest
import scala.util.Failure
import scala.Some
import actors.ExchangeAPI.ErrorResponse
import actors.ExchangeAPI.TransactionFeeResponse
import scala.util.Success
import models.CurrencyPair
import actors.ExchangeAPI.TransactionFeeRequest
import actors.btce.BtceCalculateFeeActor.Initialized
import scala.collection.immutable.Seq

object BtceCalculateFeeActor {

  def props = Props(classOf[BtceCalculateFeeActor])

  case object Initialized

}

/**
 * Exchange meta-data actor.
 * {{{
 *   https://btc-e.com/api/3/info
 * }}}
 */
class BtceCalculateFeeActor extends Actor with ActorLogging with UnboundedStash {

  val apiURL = "https://btc-e.com/api/3/info"

  implicit val system = context.system

  import system.dispatcher

  implicit val unmarshaller = new FeeUnmarshaller()

  var fees = Map[CurrencyPair, BigDecimal]()

  val pipeline: HttpRequest => Future[Map[CurrencyPair, BigDecimal]] =
    sendReceive ~> unmarshal[Map[CurrencyPair, BigDecimal]]

  override def preStart() = {
    context become receiveWhenInitializing
    val responseFuture = pipeline(HttpRequest(GET, Uri(apiURL)))
    responseFuture onComplete {
      case Success(result) =>
        fees = result
        log.info("Initialized fees map")
        self ! Initialized
      case Failure(ex) =>
        log.error("Failed to initialize fees map. Seppuku!")
        self ! Kill
    }
  }

  override def receive: Receive = {
    case req @ TransactionFeeRequest(pair, exchange) =>
      log.info(s"Fee calculation request for $pair")
      sendResponse(sender, req)
    case CurrencyPairsRequest =>
      log.info("Request for all available currency pairs")
      sender ! CurrencyPairsResponse(fees.keys.to[Seq])
  }

  def receiveWhenInitializing: Actor.Receive = {
    case Initialized =>
      log.info("Unstashing pending requests")
      context.unbecome()
      unstashAll()
    case _ =>
      log.info("Not initialized yet. Postponing response.")
      stash()
  }

  def sendResponse(recipient: ActorRef, request: TransactionFeeRequest) = {
    fees.get(request.currencyPair) match {
      case Some(fee) =>
        log.info(s"Sending response to $request")
        recipient ! TransactionFeeResponse(request.currencyPair, request.exchange, fee)
      case None =>
        log.info("Sending error pending response")
        ErrorResponse(request, s"No fee information for ${request.currencyPair}")
    }
  }


  class FeeUnmarshaller extends Unmarshaller[Map[CurrencyPair, BigDecimal]] with JsonReader[Map[CurrencyPair, BigDecimal]] {

    override def read(json: JsValue): Map[CurrencyPair, BigDecimal] = {
      val pairs = json.asJsObject.fields("pairs").asJsObject
      def decode(elements: Map[String, JsValue], result: Map[CurrencyPair, BigDecimal]): Map[CurrencyPair, BigDecimal] = {
        elements.keys.toList match {
          case head :: tail =>
            head.toUpperCase.split("_").toList match {
              case primary :: secondary :: Nil =>
                val pair = CurrencyPair(primary, secondary)
                val value = elements(head)
                val feeRate = value.asJsObject.fields("fee").asInstanceOf[JsNumber].value
                decode(elements - head, result + (pair -> feeRate))
              case value => throw new IllegalArgumentException(s"Unexpected currency pair format $head")
            }
          case Nil => result
        }
      }
      val feesMap = decode(pairs.fields, Map[CurrencyPair, BigDecimal]())
      log.info("Parsed fees from API call result.")
      feesMap
    }

    override def apply(entity: HttpEntity): Deserialized[Map[CurrencyPair, BigDecimal]] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[Map[CurrencyPair, BigDecimal]](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }
  }
}
