package actors

import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import rx.lang.scala.{Subject, Observer, Subscription}
import models._
import actors.OrdersPublisherActor.PublishingStarted
import actors.OrdersPublisherActor.StartPublishing
import actors.SpreadDeduplicationActor.MarketSpreadWindowOpen

object OrdersPublisherActor {

  def props(arbitrageActor: ActorRef) = Props(new OrdersPublisherActor(arbitrageActor))

  sealed trait OrderPublisherMessage

  case class StartPublishing(userId: Int, observer: Observer[MarketSpread]) extends OrderPublisherMessage

  case class PublishingStarted(subscription: Subscription) extends OrderPublisherMessage

}



class OrdersPublisherActor(arbitrageActor: ActorRef)
  extends Actor
  with ActorLogging {

  var marketChanges: Subject[MarketSpread] = Subject()

  val marketChangesObservable = marketChanges.publish

  val subs = marketChangesObservable.connect

  override def preStart() = {
    log.info("Subscribing for spread updates")
    context.system.eventStream.subscribe(self, classOf[MarketSpreadWindowOpen])
  }

  override def receive: Receive = {
    case op: MarketSpreadWindowOpen =>
      log.info("New marketSpread hint")
      marketChanges.onNext(op.spread)
    case StartPublishing(userId, observer) =>
      log.info(s"Creating subscription for user: $userId")
      val subscription = marketChangesObservable.subscribe(observer)
      sender ! PublishingStarted(subscription)
  }

}
