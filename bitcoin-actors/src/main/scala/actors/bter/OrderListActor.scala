package actors.bter

import akka.actor.{Props, ActorLogging, Actor}
import actors.ExchangeAPI.{OrderListResponse, OrdersListRequest}
import akka.pattern.pipe
import scala.concurrent.Future
import models.{CurrencyPair, Sell, Buy, Order}
import spray.http._
import spray.client.pipelining._
import spray.httpx.unmarshalling._
import spray.json._
import scala.concurrent.ExecutionContext.Implicits.global
import spray.http.HttpRequest
import actors.ApiException
import com.typesafe.config.Config
import shapeless.syntax.std.traversable.traversableOps
import common.AuthenticatedRequestBuilder

object OrderListActor {

  def props(config: Config) = Props(classOf[OrderListActor], config)

}

class OrderListActor(config: Config)
  extends Actor
  with ActorLogging {

  val url = "https://bter.com/api/1/private/orderlist"

  val detailsUrl = "https://bter.com/api/1/private/getorder"

  val publicKey = config.getString("bter.public-key")

  val privateKey = config.getString("bter.private-key")

  implicit val orderListUnmarshaller = new OrderListUnmarshaller

  implicit val orderUnmarshaller = new OrderUnmarshaller

  val pipeline: HttpRequest => Future[List[String]] = sendReceive ~> unmarshal[List[String]]

  val detailPipeline: HttpRequest => Future[Order] = sendReceive ~> unmarshal[Order]


  override def receive = {
    case OrdersListRequest =>
      collectOrders pipeTo sender
  }

  def collectOrders: Future[OrderListResponse] = {
    val requestBuilder = new AuthenticatedRequestBuilder(url, publicKey, privateKey)
    val request = requestBuilder.buildRequest()
    val responseFuture = pipeline(request)
    val result = responseFuture.flatMap { response =>
      val orderFutures = response.map(collectOrder)
      val futureOrders = Future.sequence(orderFutures)
      futureOrders.map( orders =>
        OrderListResponse(BterExchangeActor.exchange, orders)
      )
    }
    result
  }

  def collectOrder(orderId: String): Future[Order] = {
    val requestBuilder = new AuthenticatedRequestBuilder(detailsUrl, publicKey, privateKey)
    val request = requestBuilder.buildRequest("order_id" -> orderId)
    val responseFuture = detailPipeline(request)
    responseFuture
  }


  class OrderListUnmarshaller extends Unmarshaller[List[String]] with JsonReader[List[String]] {
    import actors.JsonConversions._

    override def read(json: JsValue): List[String] = {
      val resultJs = json.asJsObject
      val success: Boolean = resultJs.fields("result")
      if (success) {
        val ordersJsArray: JsArray = resultJs.fields("orders")
        val orders = ordersJsArray.elements.map(_.fields("id").asInstanceOf[JsString].value)
        orders
      } else {
        throw new ApiException(s"Error from BTER API ${resultJs.fields("error")}")
      }
    }


    override def apply(entity: HttpEntity): Deserialized[List[String]] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[List[String]](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }

  }


  class OrderUnmarshaller extends Unmarshaller[Order] with JsonReader[Order] {
    import actors.JsonConversions._
    import actors.JsonExtensions._

    override def read(json: JsValue): Order = {
      val resultJs = json.asJsObject
      val success: Boolean = resultJs.fields("result")
      if (success) {
        val orderJs: JsArray = resultJs.fields("order")
        val order = parseOrder(orderJs)
        order
      } else {
        throw new ApiException(s"Error from BTER API ${resultJs.fields("error")}")
      }
    }

    def parseOrder(orderJs: JsValue): Order = {
      import shapeless._
      val pair: String = orderJs.fields("pair")
      Order(
        id = orderJs.fields("id"),
        exchange = BterExchangeActor.exchange,
        pair = CurrencyPair.tupled(pair.split('_').toHList[String::String::HNil].get.tupled),
        created = orderJs.fields("created"),
        orderType = orderJs.fields("ordertype").mapString {
          case "Buy" => Buy
          case "Sell" => Sell
        },
        price = orderJs.fields("price"),
        quantity = orderJs.fields("quantity"),
        originalQuantity = orderJs.fields("orig_quantity")
      )
    }

    override def apply(entity: HttpEntity): Deserialized[Order] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[Order](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }

  }

}
