package actors.bter

import akka.actor.{Props, ActorLogging, Actor}
import spray.http._
import spray.httpx.unmarshalling._
import spray.json._
import spray.http.HttpRequest
import actors.ExchangeAPI.{ExchangeOrder, ErrorResponse, MarketInfo, MarketInfoRequest}
import models.CurrencyPair
import akka.pattern.pipe
import spray.client.pipelining._
import scala.concurrent.Future

object BterDepthActor {

  def props = Props(classOf[BterDepthActor])

}

/**
 * Collects reference of exchange from the closest asks and bids.
 */
class BterDepthActor extends Actor with ActorLogging {

  val apiUrl = (pair: CurrencyPair) =>
    s"https://data.bter.com/api/1/depth/${pair.primary.toLowerCase}_${pair.secondary.toLowerCase}"

  implicit val system = context.system

  import system.dispatcher

  private val pipeline: HttpRequest => Future[HttpResponse] = sendReceive

  private def sendUpdateRequest(pair: CurrencyPair): Future[HttpResponse] = {
    pipeline(Get(apiUrl(pair)))
  }

  override def receive: Actor.Receive = {
    case req @ MarketInfoRequest(pair, _) =>
      log.info(s"Collecting offers for $pair")
      val responseFuture = sendUpdateRequest(pair)
      implicit val unmarshaller = new DepthUnmarshaller(pair)
      responseFuture.map { response =>
        val depthDeserialized = response.entity.as[MarketInfo]
        depthDeserialized match {
          case Left(ex) =>
            log.error(s"Error parsing message - ${ex} for $pair")
            log.error(response.entity.asString)
            ErrorResponse(req, ex.toString)
          case Right(depth) => {
            log.debug(s"Sending response to recipient $depth")
            depth
          }
        }
      } pipeTo sender
    }


  class DepthUnmarshaller(pair: CurrencyPair) extends Unmarshaller[MarketInfo] with JsonReader[MarketInfo] {

    import actors.JsonExtensions._

    override def read(json: JsValue): MarketInfo = {
      val ask = json.asJsObject.fields("asks") match {
        case asks: JsArray =>
          val lowestAsk = asks.elements.sortBy(
            _.asInstanceOf[JsArray].elements.head.toBigDecimal)
            .head.asInstanceOf[JsArray].elements
          val List(askPrice, askVolume) = lowestAsk
          Some(ExchangeOrder(askPrice.toBigDecimal, askVolume.toBigDecimal))
        case _ => None
      }
      val bid = json.asJsObject.fields("bids") match {
        case bids: JsArray if !bids.elements.isEmpty =>
          val highestBid = bids.elements.sortBy(
            _.asInstanceOf[JsArray].elements.head.toBigDecimal)
            .last.asInstanceOf[JsArray].elements
          val List(bidPrice, bidVolume) = highestBid
          Some(ExchangeOrder(bidPrice.toBigDecimal, bidVolume.toBigDecimal))
        case _ => None
      }
      MarketInfo(pair, "BTCE", ask, bid)
    }

    override def apply(entity: HttpEntity): Deserialized[MarketInfo] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[MarketInfo](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }
  }

}
