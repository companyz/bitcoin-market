package actors.bter

import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import actors.ExchangeBaseActor
import com.typesafe.config.Config

object BterExchangeActor {

  def props(config: Config) = Props(classOf[BterExchangeActor], config)

  val exchange = "BTER"
}

class BterExchangeActor(config: Config)
  extends ExchangeBaseActor {

  import context._

  override val depthActor = actorOf(BterDepthActor.props, "reference")

  override val feeActor = actorOf(BterCalculateFeeActor.props(config), "fee")

  override val currencyPairsActor = actorOf(BterTradingPairsActor.props, "trading-pairs")

  override val openOrdersActor = actorOf(OrderListActor.props(config))
}
