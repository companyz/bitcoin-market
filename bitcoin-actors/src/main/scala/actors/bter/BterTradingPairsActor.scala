package actors.bter

import spray.http._
import akka.actor.{ActorLogging, Props, Actor}
import akka.pattern.pipe
import spray.httpx.unmarshalling._
import spray.client.pipelining._
import spray.http.HttpRequest
import spray.json._
import models.CurrencyPair
import actors.ExchangeAPI.{CurrencyPairsRequest, CurrencyPairsResponse, MarketInfoRequest}
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.collection.immutable.Seq

object BterTradingPairsActor {
  def props = Props(classOf[BterTradingPairsActor])
}

/**
 * Fetches all trading pairs.
 */
class BterTradingPairsActor extends Actor with ActorLogging {

  val apiURL = "https://data.bter.com/api/1/pairs"

  implicit val marshaller = new CurrenciesUnmarshaller

  private val pipeline: HttpRequest => Future[Seq[CurrencyPair]] =
    sendReceive ~> unmarshal[Seq[CurrencyPair]]


  override def receive: Actor.Receive = {
    case CurrencyPairsRequest =>
      val responseFuture = pipeline(Get(apiURL))
      responseFuture.map(CurrencyPairsResponse) pipeTo sender
  }


  /**
   * {{{
   *   [
   *     "btc_cny",
   *     "ltc_cny",
   *     "bqc_cny"
   *   ]
   * }}}
   */
  class CurrenciesUnmarshaller extends Unmarshaller[Seq[CurrencyPair]] with JsonReader[Seq[CurrencyPair]] {

    override def read(json: JsValue): Seq[CurrencyPair] = {
      val currencyValues = json.asInstanceOf[JsArray]
      val pairs = currencyValues.elements.map { jsValue =>
        val pair = jsValue.asInstanceOf[JsString].value
        pair.split("_").toList match {
          case primary :: secondary :: Nil => Some(CurrencyPair(primary.toUpperCase, secondary.toUpperCase))
          case _ => None
        }
      }
      pairs.filter(_.isDefined).map(_.get)
    }

    override def apply(entity: HttpEntity): Deserialized[Seq[CurrencyPair]] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[Seq[CurrencyPair]](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }
  }

}
