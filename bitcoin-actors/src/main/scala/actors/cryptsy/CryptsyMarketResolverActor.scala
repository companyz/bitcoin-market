package actors.cryptsy

import akka.actor._
import spray.http.HttpEntity
import spray.httpx.unmarshalling._
import scala.annotation.tailrec
import spray.json._
import scala.concurrent.Future
import spray.client.pipelining._
import actors.ExchangeAPI.CurrencyPairsRequest
import spray.http.HttpRequest
import actors.cryptsy.CryptsyMarketResolverActor._
import scala.util.Failure
import scala.Some
import spray.http.HttpResponse
import scala.util.Success
import models.CurrencyPair
import actors.ExchangeAPI.CurrencyPairsResponse
import scala.collection.immutable.Seq
import spray.http.HttpRequest
import actors.cryptsy.CryptsyMarketResolverActor.MarketId
import scala.util.Failure
import scala.Some
import spray.http.HttpResponse
import actors.cryptsy.CryptsyMarketResolverActor.UnresolvedMarket
import scala.util.Success
import models.CurrencyPair
import actors.ExchangeAPI.CurrencyPairsResponse
import actors.cryptsy.CryptsyMarketResolverActor.ResolveMarketId

object CryptsyMarketResolverActor {

  def props = Props(classOf[CryptsyMarketResolverActor])

  val path = "/user/cryptsy-market-resolver"

  val name = "cryptsy-market-resolver"

  /**
   * Request for all currency mappings.
   */
  case object CurrencyMapping

  case class ResolveMarketId(currencyPair: CurrencyPair)

  case class ResolveCurrency(marketId: Int)

  case class UnresolvedMarket(currencyPair: CurrencyPair)

  case class MarketId(currencyPair: CurrencyPair, marketId: Int)

  case class CurrencyMappingResponse(pairs: List[(CurrencyPair,Int)])

}

/**
 * Resolves marketId from all cryptsy markets for given currency pair.
 * It stacks all requests that arrive before marketIds have been collected.
 * After callecting market identifiers pending responses are processed.
 */
class CryptsyMarketResolverActor
  extends Actor
  with ActorLogging {

  type CurrencyToMarket = Map[CurrencyPair,Int]

  val allMarketsURL = "http://pubapi.cryptsy.com/api.php?method=marketdatav2"

  var marketIds: CurrencyToMarket = null

  var pendingRequests: List[(ActorRef, Any)] = Nil

  import system.dispatcher

  implicit val system = context.system

  val pipeline: HttpRequest => Future[HttpResponse] = sendReceive

  implicit val unmarshaller = new MarketIdUnmarshaller


  override def preStart() = {
    //request all markets
    context become receiveWhenInitializing
    pipeline(Get(allMarketsURL)) onComplete {
      case Success(response) =>
        log.info("Got response from Cryptsy on market data")
        val marketsDeserialized = response.entity.as[CurrencyToMarket]
        marketsDeserialized match {
          case Left(ex) =>
            log.error(s"Exception $ex")
            log.error("Failed to initialize actor. Seppuku!")
            self ! Kill
          case Right(markets) =>
            log.info("Market ID information ready to query")
            marketIds = markets
            sendPendingResponses()
            context.unbecome()
        }
      case Failure(ex) =>
        log.warning(s"Unable to load markets data", ex)
        self ! Kill
    }
  }


  def sendMappings(ref: ActorRef) = {
    ref ! CurrencyMappingResponse(marketIds.toList)
  }

  /**
   * Empties the queue by sending pending responses
   */
  @tailrec
  private def sendPendingResponses(): Unit = {
    pendingRequests match {
      case head :: tail =>
        head match {
          case (recipient, ResolveMarketId(currencyPair)) =>
            sendMarketId(recipient, currencyPair)
          case (recipient, CurrencyPairsRequest) =>
            sendCurrencyPairs(recipient)
          case (recipient, CurrencyMapping) =>
            sendMappings(recipient)
        }
        pendingRequests = tail
        sendPendingResponses()
      case Nil =>
    }
  }

  def sendCurrencyPairs(recipient: ActorRef) = {
    recipient ! CurrencyPairsResponse(marketIds.keys.to[Seq])
  }

  def sendMarketId(recipient: ActorRef, currencyPair: CurrencyPair) = {
    val marketIdOption = marketIds.get(currencyPair)
    marketIdOption match {
      case Some(marketId) =>
        log.debug(s"Sending response to market resolution request for $currencyPair = $marketId")
        recipient ! MarketId(currencyPair, marketId)
      case None =>
        log.warning(s"Unresolved marketID for $currencyPair")
        recipient ! UnresolvedMarket(currencyPair)
    }
  }

  def receiveWhenInitializing: Receive = {
    case msg =>
      log.info(s"Message received while initializing. Adding to pending queue $msg")
      val pending = (sender, msg)
      pendingRequests = pendingRequests :+ pending
  }


  override def receive: Receive = {
    case ResolveMarketId(currencyPair) =>
      log.debug(s"Currency resolution request for $currencyPair")
      sendMarketId(sender, currencyPair)
    case CurrencyPairsRequest =>
      log.debug("Currency list request")
      sendCurrencyPairs(sender)
    case CurrencyMapping =>
      log.debug("sending all pairs information")
      sendMappings(sender)
  }

  class MarketIdUnmarshaller extends Unmarshaller[CurrencyToMarket] with JsonReader[CurrencyToMarket] {

    override def read(json: JsValue): CurrencyToMarket = {

      import actors.JsonConversions._

      @tailrec
      def readMarkets(fields: List[JsValue], result: CurrencyToMarket): CurrencyToMarket = {
        fields match {
          case head :: tail  =>
            val label: String = head.asJsObject.fields("label")
            val maybePair = label.split("/").toList match {
              case primary :: secondary :: Nil =>
                Some(CurrencyPair(primary, secondary))
              case _ => None
            }
            val newResult = maybePair.map { pair =>
              val marketId: Int = head.asJsObject.fields("marketid")
              result + (pair -> marketId)
            }
            readMarkets(tail, newResult.getOrElse(result))
          case Nil => result
        }
      }

      val marketObjects = json.asJsObject.fields("return").asJsObject.fields("markets").asJsObject
      readMarkets(marketObjects.fields.values.toList, Map[CurrencyPair, Int]())
    }

    override def apply(entity: HttpEntity): Deserialized[CurrencyToMarket] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[CurrencyToMarket](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal price value", ex))
      }
    }
  }

}