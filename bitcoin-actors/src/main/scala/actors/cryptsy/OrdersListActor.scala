package actors.cryptsy

import akka.actor.{ActorRef, Props, Actor, ActorLogging}
import akka.pattern.{pipe, ask}
import com.typesafe.config.Config
import models._
import org.joda.time.DateTime
import actors.ExchangeAPI._
import models.CurrencyPairConversions._
import scala.concurrent.Future
import scala.concurrent.duration._
import spray.http._
import spray.client.pipelining._
import spray.httpx.unmarshalling._
import spray.json._
import scala.concurrent.ExecutionContext.Implicits.global
import spray.http.HttpRequest
import models.Order
import actors.ApiException
import actors.cryptsy.CryptsyMarketResolverActor.{CurrencyMappingResponse, CurrencyMapping, MarketId, ResolveCurrency}
import akka.util.Timeout
import models.CurrencyPairConversions._

object OrdersListActor {

  def props(config: Config, marketIdActor: ActorRef) = Props(classOf[OrdersListActor], config, marketIdActor)

}

/**
 * Collects current orders from Cryptsy API.
 */
class OrdersListActor(config: Config, marketIdActor: ActorRef) extends Actor with ActorLogging {

  val publicKey = config.getString("cryptsy.public-key")

  val privateKey = config.getString("cryptsy.private-key")

  var marketMapping: Map[Int, CurrencyPair] = _

  implicit val orderUnmarshaller = new OrderListUnmarshaller

  val pipeline: HttpRequest => Future[List[Order]] = sendReceive ~> unmarshal[List[Order]]

  override def preStart() = {
    implicit val timeout = Timeout(10 seconds)
    val mappingsFuture = marketIdActor ? CurrencyMapping
    mappingsFuture map {
      case CurrencyMappingResponse(mappings) =>
        log.debug("Got currency mappings")
        marketMapping = mappings.map(_.swap).toMap
    }

  }

  override def receive: Receive = {
    case OrdersListRequest =>
      collectTrades pipeTo sender
  }

  def collectTrades: Future[OrderListResponse] = {
    val requestBuilder = new CryptsyAuthenticatedRequestBuilder(publicKey, privateKey)
    val request = requestBuilder.buildRequest("allmyorders")
    val responseFuture = pipeline(request)
    responseFuture.map(OrderListResponse(CryptsyExchangeActor.exchange, _))
  }

  class OrderListUnmarshaller extends Unmarshaller[List[Order]] with JsonReader[List[Order]] {
    import actors.JsonConversions._
    import actors.JsonExtensions._

    override def read(json: JsValue): List[Order] = {
      val resultJs = json.asJsObject
      val success: Int = resultJs.fields("success")
      success match {
        case 1 =>
          val ordersJsArray: JsArray = resultJs.fields("return")
          val orders = ordersJsArray.elements.map(parseOrder)
          orders
        case _ =>
          throw new ApiException(s"Error from Cryptsy API ${resultJs.fields("error")}")
      }
    }

    def parseOrder(orderJs: JsValue): Order = {
      val currencyPair = orderJs.fields("marketId").parseUsing(marketMapping(_))
      Order(
        id = orderJs.fields("orderid"),
        exchange = CryptsyExchangeActor.exchange,
        pair = currencyPair,
        created = orderJs.fields("created"),
        orderType = orderJs.fields("ordertype").mapString {
          case "Buy" => Buy
          case "Sell" => Sell
        },
        price = orderJs.fields("price"),
        quantity = orderJs.fields("quantity"),
        originalQuantity = orderJs.fields("orig_quantity")
      )
    }

    override def apply(entity: HttpEntity): Deserialized[List[Order]] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[List[Order]](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }
  }

}
