package actors.cryptsy

import scala.compat.Platform
import spray.http.HttpHeaders._
import javax.crypto.spec.SecretKeySpec
import javax.crypto.Mac
import spray.http._
import spray.http.HttpMethods._
import spray.http.HttpHeaders.RawHeader
import spray.http.HttpRequest
import spray.http.HttpHeaders.RawHeader
import spray.client.pipelining._

/**
 * Private request builder.
 */
class CryptsyAuthenticatedRequestBuilder(
                             publicKey: String,
                             privateKey: String) {

  val url = "https://api.cryptsy.com/api"

  val keyspec: SecretKeySpec =
    new SecretKeySpec(privateKey.getBytes("UTF-8"), "HmacSHA512")

  val mac = Mac.getInstance("HmacSHA512")

  mac.init(keyspec)

  /**
   * Create data signature string
   */
  def buildRequest(method: String, data: (String, String)*): HttpRequest = {
    val dataAndNonce = ("nonce" -> Platform.currentTime.toString) +: (("method" -> method)+: data)
    val queryString = dataAndNonce.map(kv => s"${kv._1}=${kv._2}").mkString("&")

    val sign = mac.doFinal(queryString.getBytes("UTF-8")).map(b => "%02X" format (b & 0xff)).mkString
    val headers: List[HttpHeader] = List(
      RawHeader("Sign", sign.toLowerCase),
      RawHeader("Key", publicKey)
    )
    Post(url, FormData(dataAndNonce)).withHeaders(headers)
  }

}
