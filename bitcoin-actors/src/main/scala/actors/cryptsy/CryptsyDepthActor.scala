package actors.cryptsy

import actors.cryptsy.CryptsyMarketResolverActor.{MarketId, UnresolvedMarket, ResolveMarketId}
import actors.ExchangeAPI.{ExchangeOrder, MarketInfoRequest, ErrorResponse, MarketInfo}
import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import scala.concurrent.duration._
import scala.concurrent.Future
import spray.http._
import spray.client.pipelining._
import akka.pattern.{ask,pipe}
import spray.httpx.unmarshalling._
import spray.json._
import spray.http.HttpRequest
import spray.http.HttpResponse
import models.CurrencyPair
import akka.util.Timeout

object CryptsyDepthActor {

  /**
   * Creates actor Props
   * @param marketResolvingActor Reference to actor responsible for resolving market ID
   * @return Returns the Props
   */
  def props(marketResolvingActor: ActorRef) = Props(classOf[CryptsyDepthActor], marketResolvingActor)

}

/**
 * Depth calculation.
 */
class CryptsyDepthActor(marketResolvingActor: ActorRef)
  extends Actor
  with ActorLogging {

  val apiUrl = "http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=%s"

  implicit val system = context.system

  import system.dispatcher

  val key: String = "608a216b6251543388c0c8194eabfd6ff0e33409"

  val secret: String = "bef0ffd5b9523128d7d5ea0c35bbce33d26135d0cb82e53aa5ab192d67f9b3b221c62a2f4caa7b4e"

  val pipeline: HttpRequest => Future[HttpResponse] = sendReceive


  override def receive: Actor.Receive = {
    case req @ MarketInfoRequest(pair, _) =>
      log.info(s"Collecting offers for $pair")
      val marketIdFuture = resolveMarketId(pair)
      marketIdFuture flatMap {
        case Some(marketId) =>
          val request = Get(apiUrl.format(marketId))
          val responseFuture = pipeline(request)
          implicit val unmarshaller = new DepthUnmarshaller(pair)
          responseFuture.map { response =>
            val depth = response.entity.as[MarketInfo]
            depth match {
              case Left(ex) =>
                log.error(s"Error parsing message - $ex")
                log.error(response.entity.asString)
                ErrorResponse(req, ex.toString)
              case Right(depthResponse) =>
                log.debug(s"Sending response to recipient $depth")
                depthResponse
            }
          }
        case None => Future.successful(ErrorResponse(req, "Unable to resolve market ID"))
      } pipeTo sender
  }


  def resolveMarketId(pair: CurrencyPair) = {

    //expected long response if market resolving actor not initialized
    implicit val timeout = Timeout(30 seconds)

    marketResolvingActor ? ResolveMarketId(pair) map {
      case UnresolvedMarket =>
        log.warning(s"Market ID unresolved for $pair")
        None
      case MarketId(_, marketId) =>
        log.debug(s"Got market ID for $pair")
        Some(marketId)
    }
  }

  class DepthUnmarshaller(pair: CurrencyPair) extends Unmarshaller[MarketInfo] with JsonReader[MarketInfo] {

    import actors.JsonExtensions._

    def decodeOffer(value: JsValue) = {
      val fields = value.asJsObject.fields
      Map(
        "price" -> fields("price").toBigDecimal,
        "quantity" -> fields("quantity").toBigDecimal
      )
    }

    override def read(json: JsValue): MarketInfo = {

      val success = json.asJsObject.fields("success").toBigDecimal.intValue()
      success match {
        case 0 =>
          val error = json.asJsObject.fields("error").asInstanceOf[JsString].value
          throw new Exception(error)
        case _ =>
          val returnObject = json.asJsObject.fields("return").asJsObject
          val market = returnObject.fields("markets").asJsObject.fields.head._2.asJsObject
          val ask = market.fields("sellorders") match {
            case asks: JsArray if !asks.elements.isEmpty =>
              val askValues = asks.elements.map(decodeOffer)
              val lowestAsk = askValues.sortBy(_("price")).head
              Some(ExchangeOrder(lowestAsk("price"),lowestAsk("quantity")))
            case _ =>
              None
          }
          val bid = market.fields("buyorders") match {
            case bids: JsArray if !bids.elements.isEmpty =>
              val bidValues = bids.elements.map(decodeOffer)
              val highestBid = bidValues.sortBy(_("price")).last
              Some(ExchangeOrder(highestBid("price"),highestBid("quantity")))
            case _ => None
          }

          MarketInfo(pair, "Cryptsy", ask, bid)
      }
    }

    override def apply(entity: HttpEntity): Deserialized[MarketInfo] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[MarketInfo](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }
  }

}
