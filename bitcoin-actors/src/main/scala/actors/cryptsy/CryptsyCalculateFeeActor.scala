package actors.cryptsy

import akka.actor.{Props, Actor, ActorLogging}
import actors.ExchangeAPI.{TransactionFeeResponse, TransactionFeeRequest}

object CryptsyCalculateFeeActor {

  def props = Props(classOf[CryptsyCalculateFeeActor])

}

/**
 * Collects fee value from Cryptsy.
 */
class CryptsyCalculateFeeActor extends Actor with ActorLogging {

  override def receive: Actor.Receive = {
    case msg @ TransactionFeeRequest(pair, "Cryptsy") =>
      sender ! TransactionFeeResponse(pair, "Cryptsy", BigDecimal(0.2))
  }

}
