package actors.cryptsy

import models.{CurrencyPair, Ticker}
import spray.httpx.unmarshalling._
import akka.io.IO
import spray.can.Http
import spray.http.{HttpEntity, Uri, HttpRequest}
import spray.http.HttpMethods._
import spray.json._
import actors.cryptsy.CryptsyMarketResolverActor._
import org.joda.time.format.DateTimeFormat
import akka.actor.{ActorLogging, Actor, Props}

@deprecated
object CryptsyMarketDataActor {
  def props(currencyPair: CurrencyPair) = Props(new CryptsyMarketDataActor(currencyPair))

}

/**
 * Repeatedly fetches data from cryptsy.
 *
 * URL https://api.cryptsy.com/api
 */
@deprecated("No longer needed since reference functionality returns all the data needed")
class CryptsyMarketDataActor(currencyPair: CurrencyPair)
  extends Actor
  with ActorLogging {

  /**
   * Url to target market. The market is given by an identifier that needs to be collected.
   */
  var marketUrl: Option[String] = None

  import context._

  /**
   * Invoked in given interval by scheduler. The action should request data update from corresponding API.
   */
  def sendUpdateRequest(): Unit = marketUrl.map { url =>
    IO(Http) ! HttpRequest(GET, Uri(url))
  }

  override def preStart() = {
    system.actorSelection(CryptsyMarketResolverActor.path) ! ResolveMarketId(currencyPair)
  }

  override def unhandled(message: Any): Unit = {
    message match {
      case MarketId(pair, marketId) => {
        marketUrl = Some(s"http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid=$marketId")
      }
      case _ => super.unhandled(message)
    }
  }

  class TickerUnmarshaller extends Unmarshaller[Ticker] with JsonReader[Ticker] {

    /**
     * Expected date example: 2014-03-02 17:22:59
     */
    val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    /**
     * Expected response example:
     * {{{
     *  {
     *    success: 1,
     *    return: {
     *      markets: {
     *        ASC: {
     *          marketid: "112",
     *          label: "ASC/XPM",
     *          lasttradeprice: "0.00013313",
     *          volume: "129310.09752745",
     *          lasttradetime: "2014-03-09 12:43:20",
     *          primaryname: "AsicCoin",
     *          primarycode: "ASC",
     *          secondaryname: "PrimeCoin",
     *          secondarycode: "XPM",
     *          recenttrades: [{
     *              id: "29232032",
     *              time: "2014-03-09 12:43:20",
     *              price: "0.00013313",
     *              quantity: "390.73517019",
     *              total: "0.05201857"
     *            }],
     *          sellorders: [{
     *            price: "0.00014046",
     *            quantity: "4.79102145",
     *            total: "0.00067295"
     *            }],
     *          buyorders: [{
     *            price: "0.00013382",
     *            quantity: "284.53230379",
     *            total: "0.03807611"
     *          }]
     *        }
     *      }
     *    }
     *  }
     * }}}
     *
     * @param json JSON object reference
     * @return Returns Ticker object
     */
    override def read(json: JsValue): Ticker = {
      val ticker = json.asJsObject
      val success = ticker.fields("success").asInstanceOf[JsNumber].value == 1
      if(!success) {
        val error = ticker.fields("error").asInstanceOf[JsString].value
        throw new RuntimeException(s"Response error: $error")
      }
      val result = ticker.fields("return").asJsObject.fields("markets").asJsObject.fields.values.head.asJsObject
      val last = BigDecimal(result.fields("lasttradeprice").asInstanceOf[JsString].value)
      val volume = BigDecimal(result.fields("volume").asInstanceOf[JsString].value)
      val date = result.fields("lasttradetime").asInstanceOf[JsString].value

      val buy = last
      val sell = last

      Ticker(currencyPair, CryptsyExchangeActor.exchange, last, buy, sell, volume, dateFormat.parseDateTime(date))
    }

    override def apply(entity: HttpEntity): Deserialized[Ticker] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[Ticker](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal result data", ex))
      }
    }
  }

  implicit val unmarshaller = new TickerUnmarshaller

  override def receive: Actor.Receive = ???
}
