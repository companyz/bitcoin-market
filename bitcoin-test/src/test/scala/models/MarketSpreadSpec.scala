package models

import org.specs2.mutable.Specification
import actors.ExchangeAPI.{ ExchangeOrder => Ordr, MarketInfo}
import CurrencyPairConversions._

class MarketSpreadSpec extends Specification {

  "Market spread" should {

    "not be actionable when spread is 0" in {
      val marketSpread =  MarketSpread(
        MarketInfo("FOO" -> "BAR", "FAKE", ask = Some(Ordr(0.00412, 315)), bid = Some(Ordr(0.00407, 1946.828448))),
        MarketInfo("FOO" -> "BAR", "FAKE", ask = Some(Ordr(0.00411971, 13.73495735)), bid = Some(Ordr(0.00407259, 11.5566501))),
        0
      )
      marketSpread.actionable must beFalse
    }

    "not be actionable when bid is greater than ask" in {
      val marketSpread =  MarketSpread(
        MarketInfo("FOO" -> "BAR", "FAKE", ask = Some(Ordr(0.02568, 0.02800201)), bid = Some(Ordr(0.02559, 5.35))),
        MarketInfo("FOO" -> "BAR", "FAKE", ask = Some(Ordr(0.02507127, 6.92E-04)), bid = Some(Ordr(0.02541222, 37.73160152))),
        0
      )
      marketSpread.actionable must beFalse
    }


    "be actionable" in {
      val marketSpread =  MarketSpread(
        MarketInfo("FOO" -> "BAR", "FAKE", ask = Some(Ordr(2.00E-04, 2)), bid = Some(Ordr(1.90E-04, 288.4615329))),
        MarketInfo("FOO" -> "BAR", "FAKE", ask = Some(Ordr(2.12E-04, 274.5600623)), bid = Some(Ordr(2.10E-04, 27.4063373))),
        0
      )
      marketSpread.actionable must beFalse
    }

  }

}
