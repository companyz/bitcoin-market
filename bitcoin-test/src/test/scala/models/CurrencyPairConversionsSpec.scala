package models

import org.specs2.mutable.Specification
import CurrencyPairConversions._

class CurrencyPairConversionsSpec
  extends Specification {

  "CurrencyPairConversions" should {

    "convert tuple to CurrencyPair" in {
      val pair: CurrencyPair = "A" -> "B"
      pair.primary must be_==("A")
      pair.secondary must be_==("B")
    }

    "pick currencies that appear at least twice" in {
      val set1: Seq[CurrencyPair] = Seq("A" -> "B", "A" -> "C")
      val set2: Seq[CurrencyPair] = Seq("A" -> "B", "D" -> "E", "F" -> "G")
      val set3: Seq[CurrencyPair] = Seq("A" -> "B", "A" -> "C", "F" -> "G")
      val result = Seq(set1, set2, set3).filterSinglePairs
      result must containAllOf(Seq[CurrencyPair]("A" -> "B", "A" -> "C", "F" -> "G"))
    }

  }

}
