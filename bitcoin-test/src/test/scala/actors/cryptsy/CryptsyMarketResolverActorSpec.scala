package actors.cryptsy

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.{Props, ActorSystem}
import actors.cryptsy.CryptsyMarketResolverActor._

import scala.concurrent.duration._
import actors.ExchangeAPI.CurrencyPairsRequest
import actors.cryptsy.CryptsyMarketResolverActor.UnresolvedMarket
import actors.cryptsy.CryptsyMarketResolverActor.MarketId
import models.CurrencyPair
import actors.ExchangeAPI.CurrencyPairsResponse
import actors.cryptsy.CryptsyMarketResolverActor.ResolveMarketId
import scala.concurrent.Future
import spray.http.{HttpRequest, StatusCode, HttpEntity, HttpResponse}
import spray.http.ContentTypes._
import org.specs2.time.NoTimeConversions

class CryptsyMarketResolverActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike
  with NoTimeConversions {

  val json =
    """
      |{
      |  "success":1,
      |  "return":
      |  {
      |    "markets":
      |    {
      |      "BTC\/USD":{"marketid":"2","label":"BTC\/USD","lasttradeprice":"0.00000000","volume":"0.00000000","lasttradetime":"0000-00-00 00:00:00","primaryname":"BitCoin","primarycode":"BTC","secondaryname":"US Dollar","secondarycode":"USD","recenttrades":null,"sellorders":null,"buyorders":null},
      |      "DOGE\/USD":{"marketid":"182","label":"DOGE\/USD","lasttradeprice":"0.00000000","volume":"0.00000000","lasttradetime":"0000-00-00 00:00:00","primaryname":"Dogecoin","primarycode":"DOGE","secondaryname":"US Dollar","secondarycode":"USD","recenttrades":null,"sellorders":null,"buyorders":null}
      |    }
      |  }
      |}
    """.stripMargin

  "cryptsy market resolver" should {

    "Return market ID for given currency pair" in {
      val currencyPair = CurrencyPair("LTC", "BTC")
      val resolverActor = system.actorOf(CryptsyMarketResolverActor.props)

      val probe = TestProbe()

      probe.send(resolverActor, ResolveMarketId(currencyPair))
      val marketId = probe.expectMsgClass(FiniteDuration(20, SECONDS), classOf[MarketId])
      marketId.marketId must_== 3
    }

    "Respond when market is not recognized" in {
      val currencyPair = CurrencyPair("USD", "YYY")
      val resolverActor = system.actorOf(CryptsyMarketResolverActor.props)

      val probe = TestProbe()

      probe.send(resolverActor, ResolveMarketId(currencyPair))
      val response = probe.expectMsgClass(FiniteDuration(20, SECONDS), classOf[UnresolvedMarket])
      response.currencyPair must_== currencyPair
    }

    "Respond with list of currencies" in {
      val resolverActor = system.actorOf(CryptsyMarketResolverActor.props)

      val probe = TestProbe()

      probe send(resolverActor, CurrencyPairsRequest)
      val response = probe.expectMsgClass(FiniteDuration(20, SECONDS), classOf[CurrencyPairsResponse])
      response.pairs.isEmpty must beFalse
    }


    "Respond with all mappings" in {
      val resolverActor = system.actorOf(Props(new CryptsyMarketResolverActor {
        override val pipeline = (req: HttpRequest) => Future.successful(
          HttpResponse(
            status = StatusCode.int2StatusCode(200),
            entity = HttpEntity(`application/json`, json)
          )
        )
      }))

      val probe = TestProbe()

      probe send(resolverActor, CurrencyMapping)
      val response = probe.expectMsgClass(20 seconds, classOf[CurrencyMappingResponse])
      response.pairs.size must_== 2
      response.pairs.exists(elem => elem._2 == 2 && elem._1 == CurrencyPair("BTC", "USD"))
      response.pairs.exists(elem => elem._2 == 182 && elem._1 == CurrencyPair("DOGE", "USD"))
    }

  }

}
