package actors.cryptsy

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestActorRef, TestProbe, TestKit}
import akka.actor.ActorSystem
import scala.concurrent.duration._
import actors.cryptsy.CryptsyMarketResolverActor.{ResolveMarketId, MarketId}
import org.specs2.matcher.Matchers
import org.specs2.mock.Mockito
import akka.pattern.ask
import scala.concurrent.{Await, Promise}
import spray.http._
import spray.http.HttpRequest
import spray.http.HttpResponse
import actors.cryptsy.CryptsyMarketResolverActor.MarketId
import models.CurrencyPair
import actors.cryptsy.CryptsyMarketResolverActor.ResolveMarketId
import actors.ExchangeAPI.{MarketInfo, MarketInfoRequest}
import akka.util.Timeout
import models.CurrencyPairConversions._

class CryptsyDepthActorTest
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike
  with Matchers
  with Mockito {

  "Cryptsy depth API actor" should {

    "Fetch market information data from API" in {

      val probe = TestProbe()
      val marketResolvingActor = TestProbe()

      val dataActor = system.actorOf(CryptsyDepthActor.props(marketResolvingActor.ref))

      import actors.ExchangeAPI._
      probe.send(dataActor, MarketInfoRequest("LTC" -> "BTC", CryptsyExchangeActor.exchange))
      marketResolvingActor.expectMsg(FiniteDuration(5, SECONDS), ResolveMarketId("LTC" -> "BTC"))
      marketResolvingActor.reply(MarketId("LTC" -> "BTC", 3))

      val response = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[MarketInfo])
      response.ask.get.price must be_>(BigDecimal(0))
      response.ask.get.volume must be_>(BigDecimal(0))
      response.bid.get.price must be_>(BigDecimal(0))
      response.bid.get.volume must be_>(BigDecimal(0))
    }

//    "should parse mock data" in {
//
//      val json = """
//         |{
//         |  "success": 1,
//         |  "return": {
//         |    "markets": {
//         |      "MNC": {
//         |        "marketid": "7",
//         |        "label": "MNC/BTC",
//         |        "lasttradeprice": "0.00036390",
//         |        "volume": "2273.03544120",
//         |        "lasttradetime": "2014-04-06 14:56:22",
//         |        "primaryname": "MinCoin",
//         |        "primarycode": "MNC",
//         |        "secondaryname": "BitCoin",
//         |        "secondarycode": "BTC",
//         |        "recenttrades": [
//         |          {"id":"35861007","time":"2014-04-06 14:56:22","price":"0.00036390","quantity":"1.19040474","total":"0.00043319"}
//         |        ],
//         |        "sellorders": [
//         |          {"price":"0.00036649","quantity":"316.48406880","total":"0.11598825"},
//         |          {"price":"0.00036650","quantity":"0.50345720","total":"0.00018452"}
//         |        ],
//         |        "buyorders":[
//         |          {"price":"0.00036390","quantity":"691.29132430","total":"0.25156091"},
//         |          {"price":"0.00036389","quantity":"0.13345455","total":"0.00004856"}
//         |        ]
//         |      }
//         |    }
//         |  }
//         |}
//      """.stripMargin
//      val mockResponse = HttpResponse(StatusCodes.OK, HttpEntity(ContentTypes.`application/json`, json.getBytes))
//
//      val pair = CurrencyPair("LTC", "BTC")
//
//      val marketResolvingActor = TestActorRef(new CryptsyMarketResolverActor {
//        override def receive = {
//          case ResolveMarketId(aPair) => sender ! MarketId(aPair, 666)
//          case _ => log.info("What???")
//        }
//      })
//
//      val actorRef = TestActorRef(new CryptsyDepthActor(marketResolvingActor) {
//        override val pipeline =
//          (req:HttpRequest) => Promise.successful(mockResponse).future
//      })
//
//      implicit val timeout = Timeout(10000l)
//      val future = actorRef ? MarketInfoRequest(pair, "Cryptsy")
//      val response = Await.result(future, Duration(10, SECONDS)).asInstanceOf[MarketInfo]
//
//      response.exchange must be_==("Cryptsy")
//      response.ask.get.price must be_==(0.00036389)
//      response.bid.get.price must be_==(0.00036650)
//    }

  }
}
