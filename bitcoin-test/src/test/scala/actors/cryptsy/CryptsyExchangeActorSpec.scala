package actors.cryptsy

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import models.CurrencyPair
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import models.CurrencyPairConversions._

class CryptsyExchangeActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike {


  "Cryptsy Exchange actor" should {
    "Fetch reference data from API" in {
      val config = ConfigFactory.empty()
      val exchangeActor = system.actorOf(CryptsyExchangeActor.props(config))

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(exchangeActor, MarketInfoRequest("LTC" -> "BTC", CryptsyExchangeActor.exchange))

      val response = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[MarketInfo])
      response.currencyPair must_== CurrencyPair("LTC", "BTC")
    }
  }

}
