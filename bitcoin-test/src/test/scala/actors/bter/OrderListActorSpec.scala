package actors.bter

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory
import scala.concurrent.Future
import spray.http.HttpRequest
import org.specs2.time.NoTimeConversions
import actors.ExchangeAPI.{OrderListResponse, OrdersListRequest}
import scala.concurrent.duration._
import models.{Buy, CurrencyPair, Order}
import org.joda.time.DateTime

class OrderListActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike
  with NoTimeConversions {

  "Order list" should {

    "give list of orders" in {
      val config = ConfigFactory.parseString(
        """
          |bter.public-key = "abc"
          |bter.private-key = "123"
        """.stripMargin)
      val order1 = Order("1", "BTER", CurrencyPair("BTC", "LTC"), DateTime.now, Buy, 1.0, 2.0, 3.0)
      val actor = system.actorOf(Props(new OrderListActor(config) {
        override val pipeline = (req: HttpRequest) => Future.successful(
          List("1", "2")
        )

        override val detailPipeline = (req: HttpRequest) => Future.successful(
          order1
        )
      }))
      val probe = TestProbe()
      probe.send(actor, OrdersListRequest)
      val response = probe.expectMsgClass(20 seconds, classOf[OrderListResponse])
      response.exchange must be_==("BTER")
      response.orders.size must be_==(2)
      response.orders.head must be_==(order1)
    }

  }

}
