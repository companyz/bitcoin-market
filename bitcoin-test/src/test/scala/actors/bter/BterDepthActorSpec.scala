package actors.bter

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import models.CurrencyPair
import scala.concurrent.duration._

class BterDepthActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike {


  "BER reference actor" should {

    "Fetch reference data from API" in {
      val dataActor = system.actorOf(BterDepthActor.props)

      import actors.ExchangeAPI._
      import models.CurrencyPairConversions._

      val probe = TestProbe()
      probe.send(dataActor, MarketInfoRequest("BTC" -> "CNY", BterExchangeActor.exchange))

      val response = probe.expectMsgClass(FiniteDuration(30, SECONDS), classOf[MarketInfo])
      response.currencyPair must_== CurrencyPair("BTC", "CNY")
    }


    "parse malformed data" in {
      val dataActor = system.actorOf(BterDepthActor.props)

      import actors.ExchangeAPI._
      import models.CurrencyPairConversions._

      val probe = TestProbe()
      probe.send(dataActor, MarketInfoRequest("DGC" -> "BTC", BterExchangeActor.exchange))

      val response = probe.expectMsgClass(FiniteDuration(30, SECONDS), classOf[MarketInfo])
      response.currencyPair must_== CurrencyPair("DGC", "BTC")
    }

  }

}
