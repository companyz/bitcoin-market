package actors.bter

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import models.CurrencyPair
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import models.CurrencyPairConversions._

class BterExchangeActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike {


  sequential

  "BTER Exchange actor" should {
    "Fetch reference data from API" in {
      val config = ConfigFactory.load()
      val exchangeActor = system.actorOf(BterExchangeActor.props(config))

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(exchangeActor, MarketInfoRequest("LTC" -> "BTC", BterExchangeActor.exchange))

      val response = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[MarketInfo])
      response.currencyPair must_== CurrencyPair("LTC", "BTC")
    }

    "Fetch fee data from API" in {
      val config = ConfigFactory.parseString(
        """
          |bter.fees.default=0.6
        """.stripMargin)
      val exchangeActor = system.actorOf(BterExchangeActor.props(config))

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(exchangeActor, TransactionFeeRequest("LTC" -> "BTC", BterExchangeActor.exchange))

      val response = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[TransactionFeeResponse])
      response.currencyPair must_== CurrencyPair("LTC", "BTC")
      response.fee must_== BigDecimal(0.6)
    }
  }

}
