package actors.bter

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import models.CurrencyPair
import scala.concurrent.duration._

class BterTradingPairsActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike {


  "BER trading pairs actor" should {
    "return currencies" in {
      val dataActor = system.actorOf(BterTradingPairsActor.props)

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(dataActor, CurrencyPairsRequest)

      val response = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[CurrencyPairsResponse])
      response.pairs.contains(CurrencyPair("BTC", "CNY")) must beTrue
    }
  }
}