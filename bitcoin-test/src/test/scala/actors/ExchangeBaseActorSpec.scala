package actors

import org.specs2.mutable.{SpecificationLike, Specification}
import akka.actor.{Props, ActorSystem, ActorRef}
import akka.testkit.{TestKit, TestProbe}
import actors.ExchangeAPI.TransactionFeeRequest
import models.CurrencyPair
import com.typesafe.config.ConfigFactory

class ExchangeBaseActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike  {

  class ActorMock(implicit system: ActorSystem) {
    val depthProbe = new TestProbe(system)
    val currencyPairProbe = new TestProbe(system)
    val feeProbe = new TestProbe(system)
    val orderListProbe = new TestProbe(system)

    val actor = system.actorOf(Props(new ExchangeBaseActor {

      override val depthActor: ActorRef = depthProbe.testActor

      override val currencyPairsActor: ActorRef = currencyPairProbe.testActor

      override val feeActor: ActorRef = feeProbe.testActor

      override val openOrdersActor = orderListProbe.testActor
    }))

  }

  "Exchange actor" should {

    "return fee value" in {
      val actorMock = new ActorMock()

      val msg = TransactionFeeRequest(CurrencyPair("BTC", "LTC"), "TEST")
      actorMock.actor ! msg
      val result = actorMock.feeProbe.expectMsg(msg)

      result.currencyPair must_== msg.currencyPair
    }

  }

}
