package actors.btce

import org.specs2.mutable.SpecificationLike
import models.CurrencyPair
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import scala.concurrent.duration._
import models.CurrencyPairConversions._

class BtceDepthActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike {


  "Btce reference actor" should {
    "Fetch reference data from API" in {
      val dataActor = system.actorOf(BtceDepthActor.props)

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(dataActor, MarketInfoRequest("BTC" -> "USD", BtceExchangeActor.exchange))

      val response = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[MarketInfo])
      response.currencyPair must_== CurrencyPair("BTC", "USD")
    }
  }

}
