package actors.mint

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import org.specs2.matcher.Matchers
import org.specs2.time.NoTimeConversions
import actors.ExchangeAPI.{CurrencyPairsResponse, CurrencyPairsRequest}
import scala.concurrent.duration._

class TradingPairsActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike
  with Matchers
  with NoTimeConversions {

  "TradingPairsActor" should {

    "return currency pairs" in {
      val actor = system.actorOf(TradingPairsActor.props)
      val probe = new TestProbe(system)
      probe.send(actor, CurrencyPairsRequest)
      val response = probe.expectMsgClass(20 seconds, classOf[CurrencyPairsResponse])
      response.pairs.size must be_>(0)
    }

  }

}
